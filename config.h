//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/		/*Command*/				/*Update Interval*/		/*Update Signal*/
	{"",			"cpu",					10,				4},
	{"", 			"memory",	        		6,		             	6}, 
	/* {"",			"nettraf",				1,				8}, */
	{"",			"volume",				0,				2},
	/*{"",			"sb-battery",				5,				3}, */
	{"", 			"kblayout", 				0, 				3}, 
	{"",			"clock",				60,				10},
	
	
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = " | ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
